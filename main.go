package main

import (
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gocolly/colly"
)

var readmeList []string

func createFile(readmeList []string) error {
	f, err := os.Create("all_readme.txt")

	if err != nil {
		return err
	}

	defer f.Close()

	for _, line := range readmeList {
		_, err = f.WriteString(line)
		if err != nil {
			return err
		}
	}

	return nil
}

func fileDowndloader(url string) (*string, error) {
	log.Println("fileDowndloader has been launched ...")

	// Get the data from url
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// read the body value
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// convert array of bytes in string
	bodyString := string(bodyBytes)

	log.Println("fileDowndloader done")
	return &bodyString, nil
}

func readmeScrapper(arg, link string) *string {
	log.Println("Downloading content from README file ...")

	url := arg + link

	log.Printf("Get README from : %v\n", url)

	// get content of readme in string format
	content, err := fileDowndloader(url)
	if err != nil {
		log.Fatalf(err.Error())
	}

	return content
}

func parsePathList(arg *string, pathList []string) {
	for _, path := range pathList {
		if path == "README" {
			readme := readmeScrapper(*arg, path)
			readmeList = append(readmeList, *readme)
		} else {
			// get new arg with new path
			newArg := *arg + path

			// get all href value from new path
			newPathList, err := getPageAnchors(&newArg)
			if err != nil {
				log.Fatalf(err.Error())
			}
			// relaunch parsing of path list recurcively
			// TODO: use goroutine here for scrap faster
			parsePathList(&newArg, newPathList)
		}
	}
}

func getPageAnchors(arg *string) ([]string, error) {
	var pathList []string

	c := colly.NewCollector()

	// get value of each href in anchors balises
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		// get the content of one href anchor
		link := e.Attr("href")

		if link != "../" {
			pathList = append(pathList, link)
		}
	})

	c.OnRequest(func(r *colly.Request) {
		log.Println("Visiting", r.URL)
	})

	c.Visit(*arg)

	return pathList, nil
}

func main() {
	// get url to scrap
	arg := os.Args[1]

	// get the first page
	pathList, err := getPageAnchors(&arg)
	if err != nil {
		log.Fatalf(err.Error())
	}

	// launch parsing
	parsePathList(&arg, pathList)

	// create file
	createFile(readmeList)
}
